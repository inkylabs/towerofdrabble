#!/usr/bin/env python
#
#   Copyright 2011 Inkylabs et al.
#
#   Licensed under the Apache License, Version 2.0 (the "License");
#   you may not use this file except in compliance with the License.
#   You may obtain a copy of the License at
#
#       http://www.apache.org/licenses/LICENSE-2.0
#
#   Unless required by applicable law or agreed to in writing, software
#   distributed under the License is distributed on an "AS IS" BASIS,
#   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
#   See the License for the specific language governing permissions and
#   limitations under the License.
#
from .strings import FYEL, HC, RS, elements, weapon_names

from random import choice, randint


def generate_weapon(story):
    return Weapon(damage=max(1, story + randint(-2, 2)),
                  name=choice(weapon_names), element=choice(elements))


class Weapon(object):
    def __init__(self, damage=1, name='SLINGSHOT', element='fire'):
        self.damage = damage
        self.name = name
        self.element = element

    def __str__(self):
        return (HC + FYEL +
                '%s %s (dmg %d)' % (self.element, self.name, self.damage)
                + RS)

    def fancy_str(self):
        return str(self)

    def serialize(self):
        return {
            'damage': self.damage,
            'name': self.name,
            'element': self.element,
            'type': 'weapon',
        }

    def deserialize(self, obj):
        self.damage = obj['damage']
        self.name = obj['name']
        self.element = obj['element']
        return self
