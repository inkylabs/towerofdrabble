#
#   Copyright 2011 Inkylabs et al.
#
#   Licensed under the Apache License, Version 2.0 (the "License");
#   you may not use this file except in compliance with the License.
#   You may obtain a copy of the License at
#
#       http://www.apache.org/licenses/LICENSE-2.0
#
#   Unless required by applicable law or agreed to in writing, software
#   distributed under the License is distributed on an "AS IS" BASIS,
#   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
#   See the License for the specific language governing permissions and
#   limitations under the License.
#
from __future__ import print_function

from .armor import Armor
from .barrel import Barrel
from .floor import Floor, generate_floor
from .monster import Monster
from .settings import (MAX_STORY, game_file_names, readable_game_file,
                       writable_game_file, WAIT_TIME)
from .strings import (attack_no_target, attack_miss, attack_hit,
                        player_death, level_choices)
from .weapon import Weapon

from json import dump as json_dump, load as json_load
from math import sqrt
from random import choice, random, randint
from readline import parse_and_bind, set_completer
from time import sleep
from heapq import (heapify, heappush, heappop, heappushpop)

#======================================#
#           Display Commands           #
#======================================#
def inventory_cmd(player, floors, context, *args):
    for item in player.inventory:
        if item in player.equipped:
            print('  ', item.fancy_str(), '(E)')
        else:
            print('  ', item.fancy_str())


def look_cmd(player, floors, context, *args):
    print("You are in a room.  It's roomy")
    player.floor.display(player.room)
    if len(player.room.monsters) != 0:
        print('Monsters:')
        for monster in player.room.monsters:
            print(' ', monster.fancy_str())
    if len(player.room.items) != 0:
        print('Items:')
        for item in player.room.items:
            print(' ', item.fancy_str())
    if len(player.room.barrels) != 0:
        print('Barrel:')
        for barrel in player.room.barrels:
            print(' ', barrel.fancy_str())


def status_cmd(player, floors, context, *args):
    equipped_list = ([player.weapon] + player.armor if player.weapon
                     else player.armor)
    print(player.fancy_str())
    print('Stats:')
    print('  Strength:', player.speed)
    print('  Defense:', player.defense)
    print('  Speed:', player.speed)
    display_list(equipped_list, context, 'status')


#======================================#
#         Free Action Commands         #
#======================================#
def drop_cmd(player, floors, context, *args):
    nums = parse_args(player.inventory, context, 'drop', args)
    if not nums:
        return
    nums = list(reversed(sorted(set(nums))))
    for num in nums:
        item = player.inventory[num]
        player.inventory.pop(num)
        player.room.items.append(item)
        if item in player.equipped:
            if type(item) == Weapon:
                player.weapon = None
            else:
                player.armor.remove(item)
        print(item.name + ' dropped.')


def equip_cmd(player, floors, context, *args):
    equippable = []
    for item in player.inventory:
        if item not in player.equipped:
            equippable.append(item)
    nums = parse_args(equippable, context, 'equip', args)
    if not nums:
        return
    for num in nums:
        item = equippable[num]
        if type(item) == Weapon:
            player.weapon = item
        elif type(item) == Armor:
            if len(player.armor) >= player.max_armor:
                print('You can only wear %d pieces of armor at a time.  '
                      'Use the "unequip" command\nto take one off first.'
                      % player.max_armor)
                return
            player.armor.append(item)
        print(item.name + ' equipped')


def level_cmd(player, floors, context, *args):
    if player.level_ups == 0:
        return print("You don't have any level ups")
    if player.level_ups < len(args):
        return print('Hold your horses, you only have %d level ups left.'
                        % player.level_ups)
    nums = parse_args(level_choices, context, 'level', args)
    if not nums:
        return
    for num in nums:
        if num == 0:
            player.health += 10 + randint(0, 5)
        elif num == 1:
            player.strength += 2 + randint(0, 1)
        elif num == 2:
            player.defense += 2 + randint(0, 1)
        elif num == 3:
            player.speed += 2 + randint(0, 1)
        player.level_ups -= 1


def take_cmd(player, floors, context, *args):
    nums = parse_args(player.room.items, context, 'take', args)
    if not nums:
        return
    nums = list(reversed(sorted(set(nums))))
    for num in nums:
        item = player.room.items[num]
        player.inventory.append(item)
        player.room.items.pop(num)
        print(item.name + ' taken.')
        # Update player stats
        player.stats['items taken'] += 1


def unequip_cmd(player, floors, context, *args):
    nums = parse_args(player.equipped, context, 'unequip', args)
    if not nums:
        return
    nums = list(reversed(sorted(set(nums))))
    for num in nums:
        player.unequip(num)


#======================================#
#            Turn Commands             #
#======================================#
def set_turn_order(player, context):
    chars = [player] + player.room.monsters
    context.turn_order = [[0, x] for x in chars]
    heapify(context.turn_order)


def turn(func):
    def new_func(player, floors, context, *args):
        if not func(player, floors, context, *args):
            return
        if auto_round(player, context):
            return {'quit': True}
        return final_room_check(player)
    return new_func


def movement(func):
    def new_func(player, floors, context, *args):
        if not func(player, floors, context, *args):
            return
        set_turn_order(player, context)
        return True
    return new_func


@turn
def attack_cmd(player, floors, context, *args):
    if not player.weapon:
        return print('Arm yourself, fool!')
    if len(args) > 1:
        return print('Hold on buckaroo.  One at a time now')
    possibles = player.room.monsters + player.room.barrels
    nums = parse_args(possibles, context, 'attack', args)
    if not nums:
        return
    target = possibles[nums[0]]
    damage_dealt = 0
    if type(target) == Barrel:
        player.room.barrels.remove(target)
        print('* * *BOOM!* * *')
        for monster in player.room.monsters:
            damage_dealt += monster.take_damage(target.damage, target.element)
            print(monster.fancy_str())
    elif type(target) == Monster:
        damage_dealt += target.take_damage(player.damage, target.element)
        print(choice(attack_hit) % target.name)
        print(target.fancy_str())

    exp = 0
    for monster in player.room.monsters:
        # Monster  died
        if monster.health <= 0:
            print('The %s died' % monster.name)
            # Test if monster drops weapon
            if random() < .3 and monster.weapon:
                player.room.items.append(monster.weapon)
                print('The %s dropped his %s' % (monster.name,
                      monster.weapon.fancy_str()))
            exp += player.get_kill_exp(monster)
            player.stats['monsters killed'] += 1
            player.room.monsters.remove(monster)
            for i in range(len(context.turn_order)):
                if context.turn_order[i][1] == monster:
                    context.turn_order.pop(i)
                    heapify(context.turn_order)
                    break

    # Update player stats
    player.stats['damage dealt'] += damage_dealt
    player.stats['most damage with one blow'] = max(damage_dealt,
                 player.stats['most damage with one blow'])
    player.increase_exp(exp)
    return True


@turn
def rest_cmd(player, floors, context, *args):
    if len(player.room.monsters) != 0:
        return print("Cannot rest when enemies are present.")
    rest_responses = ['Breathe in... breathe out...']
    print(choice(rest_responses))
    player.health = player.max_health
    print(player.fancy_str())
    # Update player stats
    player.stats['rests taken'] += 1
    return True


@turn
@movement
def down_cmd(player, floors, context, *args):
    if not player.room.has_down:
        return print("You can't move downstairs.")
    attempt_stairs(player, floors, -1)
    return True


@turn
@movement
def up_cmd(player, floors, context, *args):
    if not player.room.has_up:
        return print("You can't move upstairs.")
    attempt_stairs(player, floors, 1)
    # Update player stats
    player.stats['highest floor'] = max(player.floor.story,
                                        player.stats['highest floor'])
    return True


@turn
@movement
def east_cmd(player, floors, context, *args):
    if not player.room.has_east:
        return print("You can't move east")
    pos = player.room.pos
    attempt_move(player, pos[0] + 1, pos[1])
    return True


@turn
@movement
def north_cmd(player, floors, context, *args):
    if not player.room.has_north:
        return print("You can't move north")
    pos = player.room.pos
    attempt_move(player, pos[0], pos[1] - 1)
    return True


@turn
@movement
def south_cmd(player, floors, context, *args):
    if not player.room.has_south:
        return print("You can't move south")
    pos = player.room.pos
    attempt_move(player, pos[0], pos[1] + 1)
    return True


@turn
@movement
def west_cmd(player, floors, context, *args):
    if not player.room.has_west:
        return print("You can't move west")
    pos = player.room.pos
    attempt_move(player, pos[0] - 1, pos[1])
    return True


#======================================#
#            System Commands           #
#======================================#
def help_cmd(player, floors, context, *args):
    if len(args) == 0:
        print('== available comands ==')
        for name in sorted(CMDS.keys()):
            print('%s: %s' % (name, CMDS[name]['desc']))
        return
    cmd = args[0]
    if cmd not in CMDS:
        if cmd in ALIASES:
            cmd = ALIASES[cmd]
        else:
            print('%s is not a valid command.' % cmd)
            return
    print('%s: %s' % (cmd, CMDS[cmd]['desc']))


def load_cmd(player, floors, context, *args):
    if len(args) > 1:
        return print(CMDS['load']['usage'])
    available = list(game_file_names())
    nums = parse_args(available, context, 'load', args)
    if not nums:
        return
    name = available[nums[0]]
    game = json_load(readable_game_file(name))
    del floors[:]
    for i, floor in enumerate(game['floors']):
        floors.append(Floor().deserialize(floor, i + 1))
    player.deserialize(game['player'], floors)
    print('Loaded', name)


def save_cmd(player, floors, context, *args):
    if len(args) != 1:
        print('Usage: "save [name]"')
        return
    if not args[0].isalnum():
        print('Please use alphanumeric names')
        return
    game = {}
    game['player'] = player.serialize()
    game['floors'] = []
    for floor in floors:
        game['floors'].append(floor.serialize())
    f = writable_game_file(args[0])
    json_dump(game, f)
    f.close()
    print('Saved', args[0])


def set_cmd(player, floors, context, *args):
    import settings
    if len(args) != 2:
        print('Please say "set [option] [value]".')
        return
    valid_vars = ['instant']
    if args[0] not in valid_vars:
        print("You can't set that.")
        return
    true_vals = ['t', 'true']
    false_vals = ['f', 'false']
    if args[1].lower() not in true_vals and args[1].lower() not in false_vals:
        print('Invalid value for %s' % args[0])
        return
    if args[0] == 'instant':
        if args[1] in true_vals:
            settings.WAIT_TIME = 0
            print('Instant set to True')
        else:
            settings.WAIT_TIME = .5
            print('Instant set to False')


#======================================#
#          Auxilary Functions          #
#======================================#
def parse_args(list1, context, cmd, args):
    if not args:
        return display_list(list1, context, cmd)
    elements = []
    for arg in args:
        if not arg.isdigit():
            return print(CMDS[cmd]['usage'])
        index = int(arg) - 1
        if not 0 <= index < len(list1):
            return print(CMDS[cmd]['invalid'])
        elements.append(index)
    return elements


def display_list(list1, context, command):
    #Handle list not having elements
    if not list1:
        responses = CMDS[command]['none']
        print(choice(responses))
        return
    #Print header
    responses = CMDS[command]['has']
    print(choice(responses))
    #Print elements
    for i, o in enumerate(list1):
        i += 1
        s = getattr(o, 'fancy_str', o.__str__)()
        print('%2d: %s' % (i, s))


def attempt_move(player, x, y):
    if not escape(player):
        return False
    player.change_room(player.floor.rooms[x][y])
    return True


def attempt_stairs(player, floors, move):
    if not escape(player):
        return False
    player.floor = get_floor(floors, player.floor.story + move)
    if move < 0:
        player.change_room(player.floor.up_room)
    else:
        player.change_room(player.floor.down_room)
    print('Floor %d' % player.floor.story)
    return True


def escape(player):
    for monster in player.room.monsters:
        if monster.stop_escape(player):
            print('%s stops your escape!' % monster)
            return False
    return True


def get_floor(floors, n):
    num = len(floors)
    for k in range(num, n + 1):
        floors.append(generate_floor(k + 1))
    return floors[n - 1]


def auto_round(player, context):
    if not context.turn_order:
        set_turn_order(player, context)
    element = heappop(context.turn_order)
    actor = element[1]
    while actor != player:
        actor.attack(player)
        if player.health <= 0:
            player_died(player, actor)
            return True
        element[0] += 1 / sqrt(actor.speed)
        element = heappushpop(context.turn_order, element)
        actor = element[1]
    element[0] += 1 / sqrt(actor.speed)
    heappush(context.turn_order, element)


def final_room_check(player):
    if player.floor.story == MAX_STORY and player.room == player.floor.up_room:
        print('You have made it to the final room!')
        return {'quit': True}


def player_died(player, monster):
    print(choice(player_death) % monster.name)
    for key, val in player.stats.items():
        print('    %s: %d' % (key, val))


#======================================#
#           Global Variables           #
#======================================#
CMDS = {
    'attack': {
        'desc': 'attack a target',
        'func': attack_cmd,
        'has' : ['Available targets:'],
        'invalid': 'Invalid target number',
        'none': attack_no_target,
        'usage': 'Usage: attack [target_num]',
    },
    'down': {
        'desc': 'go downstairs',
        'func': down_cmd,
    },
    'drop': {
        'desc': 'drop item',
        'func': drop_cmd,
        'has' : ['Your items:'],
        'invalid': 'Invalid item number',
        'none': ['You have no items to drop.'],
        'usage': 'Usage: drop [item number]',
    },
    'east': {
        'desc': 'move east',
        'func': east_cmd,
    },
    'equip': {
        'desc': 'equip item',
        'func': equip_cmd,
        'has' : ['Items available:'],
        'invalid': 'Invalid item number',
        'none': ["You have nothing you can equip."],
        'usage': 'Usage: equip [item numbers]',
    },
    'help': {
        'desc': 'show this help message',
        'func': help_cmd,
    },
    'inventory': {
        'desc': 'show inventory',
        'func': inventory_cmd,
        'has' : ['Items in inventory:'],
        'none': ['Traveling light, I see.  Your inventory is empty.'],
    },
    'level': {
        'desc': 'level up',
        'func': level_cmd,
        'has' : ['Available choices:'],
        'invalid': 'Invalid choice',
        'none' : ["You're at max level!"],
        'usage': 'Usage: level [choices]',
    },
    'load': {
        'desc': 'load a saved game',
        'func': load_cmd,
        'has' : ['Available saves:'],
        'invalid': 'Invalid game number',
        'none': ['No available game saves'],
        'usage': 'Usage: "load [game_num]"',
    },
    'look': {
        'desc': 'look around a room or at an object',
        'func': look_cmd,
    },
    'north': {
        'desc': 'move north',
        'func': north_cmd,
    },
    'rest': {
        'desc': 'gain health',
        'func': rest_cmd,
    },
    'save': {
        'desc': 'save your game',
        'func': save_cmd,
    },
    'set': {
        'desc': 'set options',
        'func': set_cmd,
    },
    'south': {
        'desc': 'move south',
        'func': south_cmd,
    },
    'status': {
        'desc': 'show status',
        'func': status_cmd,
        'has' : ['Equipped:'],
        'none': ['No items equipped.']
    },
    'take': {
        'desc': 'take object',
        'func': take_cmd,
        'has' : ['Items available:'],
        'invalid': 'Invalid item number',
        'none': ["There's nothing to take in here."],
        'usage': 'Usage: take [item numbers]',
    },
    'west': {
        'desc': 'move west',
        'func': west_cmd,
    },
    'unequip': {
        'desc': 'unequip item',
        'func': unequip_cmd,
        'has' : ['Items equipped:'],
        'invalid': 'Invalid item number',
        'none': ["You have nothing equipped."],
        'usage': 'Usage: unequip [item numbers]',
    },
    'up': {
        'desc': 'go upstairs',
        'func': up_cmd,
    },
}


ALIASES = {
    'a': 'attack',
    'd': 'down',
    'e': 'east',
    'h': 'help',
    'i': 'inventory',
    'l': 'look',
    'n': 'north',
    'r': 'rest',
    's': 'south',
    't': 'take',
    'w': 'west',
    'u': 'up',
}


class Context(object):
    turn_order = []
    def __init__(self):
        self.previous_cmd = ''

    def translate(self, cmd, *args):
        if cmd.isdigit() and 'has' in CMDS.get(self.previous_cmd, {}):
            return self.previous_cmd, [cmd] + list(args)
        return cmd, args


class Completer(object):
    def __init__(self):
        self.prefix = None

    def complete(self, prefix, index):
        if prefix != self.prefix:
            self.prefix = prefix
            self.matches = []
            for name in sorted(CMDS.keys()):
                if name.startswith(prefix):
                    self.matches.append(name)
        try:
            return self.matches[index] + ' '
        except IndexError:
            return None


completer = Completer()
parse_and_bind('tab: complete')
set_completer(completer.complete)
