#!/usr/bin/env python
#
#   Copyright 2011 Inkylabs et al.
#
#   Licensed under the Apache License, Version 2.0 (the "License");
#   you may not use this file except in compliance with the License.
#   You may obtain a copy of the License at
#
#       http://www.apache.org/licenses/LICENSE-2.0
#
#   Unless required by applicable law or agreed to in writing, software
#   distributed under the License is distributed on an "AS IS" BASIS,
#   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
#   See the License for the specific language governing permissions and
#   limitations under the License.
#
from .armor import generate_armor
from .barrel import Barrel, generate_barrel
from .monster import Monster, generate_monster
from .settings import MAX_STORY
from .utils import deserialize_item
from .weapon import generate_weapon

from random import gauss, random


def generate_room(story, (x, y)):
    room = Room(story, (x, y))
    if random() < .15:
        for i in range(1 + abs(int(gauss(.1, .8)))):
            room.items.append(generate_weapon(story))
    if random() < .15:
        for i in range(1 + abs(int(gauss(.1, .8)))):
            room.items.append(generate_armor(story))
    for i in range(abs(int(gauss(.1, .8)))):
        room.barrels.append(generate_barrel(story))
    if random () < .25:
        for i in range(1 + abs(int(gauss(0, 1.5 + story / MAX_STORY)/2))):
            m = generate_monster(story)
            room.monsters.append(m)
            m.room = room
    return room


class Room(object):
    def __init__(self, story=1, pos=(0, 0), items=None, monsters=None,
                 barrels=None):
        self.story = story
        self.pos = pos
        #TODO: this should really be items...
        self.items = items or []
        self.monsters = monsters or []
        self.barrels = barrels or []
        self.has_north = False
        self.has_south = False
        self.has_east = False
        self.has_west = False
        self.has_up = False
        self.has_down = False

    def serialize(self):
        return {
            'items': [x.serialize() for x in self.items],
            'barrels': [x.serialize() for x in self.barrels],
            'monsters': [x.serialize() for x in self.monsters],
            'has_north': self.has_north,
            'has_south': self.has_south,
            'has_east': self.has_east,
            'has_west': self.has_north,
            'has_up': self.has_up,
            'has_down': self.has_down,
        }

    def deserialize(self, obj, floor, pos):
        self.story = floor.story
        self.pos = pos
        self.items = [deserialize_item(i) for i in obj['items']]
        self.monsters = [Monster().deserialize(x, self, floor)
                         for x in obj['monsters']]
        self.barrels = [Barrel().deserialize(x) for x in obj['barrels']]
        self.has_north = obj['has_north']
        self.has_south = obj['has_south']
        self.has_east = obj['has_east']
        self.has_west = obj['has_west']
        self.has_up = obj['has_up']
        self.has_down = obj['has_down']
        return self
