#
#   Copyright 2011 Inkylabs et al.
#
#   Licensed under the Apache License, Version 2.0 (the "License");
#   you may not use this file except in compliance with the License.
#   You may obtain a copy of the License at
#
#       http://www.apache.org/licenses/LICENSE-2.0
#
#   Unless required by applicable law or agreed to in writing, software
#   distributed under the License is distributed on an "AS IS" BASIS,
#   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
#   See the License for the specific language governing permissions and
#   limitations under the License.
#
from __future__ import print_function

from .cmd import ALIASES, CMDS, Context
from .floor import generate_floor
from .monster import generate_monster
from .player import Player
from .strings import FMAG, HC, RS
from .weapon import generate_weapon


PROMPT = '> '


class Game(object):
    def __init__(self, name=None):
        self.player = Player(name=name)
        self.player.weapon = generate_weapon(1)
        self.player.inventory.append(self.player.weapon)

    def run(self):
        floor = generate_floor(1)
        floors = [floor]
        self.player.floor = floor
        self.player.room = floor.down_room
        import settings
        if settings.DEBUG:
            self.player.room.weapons = [generate_weapon(1)]
            self.player.room.monsters = [generate_monster(1) for i in range(2)]
        context = Context()
        if not self.player.name:
            print('Welcome to the tower of drabble!')
            print('What is your name?')
            self.player.name = raw_input('> ')
        print(str(self.player) + ',')
        print('we are just receiving word that you are...')
        print(HC + FMAG + "The Prophecied Legend Of Destiny's Fate Foretold",
              'In Ages Past!' + RS)
        print('Make your way to the top of the tower.')
        print('Type "help" if you need some.')
        while True:
            # Get the command
            try:
                argv = raw_input(PROMPT).strip().split()
            except (EOFError, KeyboardInterrupt):
                print('')
                print("Later 'gater")
                return

            # Make sure the command is valid
            if len(argv) < 1:
                print('You need a command')
                continue
            cmd, args = ALIASES.get(argv[0], argv[0]), argv[1:]
            cmd, args = context.translate(cmd, *args)
            if cmd not in CMDS:
                print('"%s" is not a valid command' % cmd)
                continue

            # Run the command
            results = CMDS[cmd]['func'](self.player, floors, context, *args)
            context.previous_cmd = cmd
            if results:
                if results.get('quit', False):
                    return
