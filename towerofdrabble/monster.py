#
#   Copyright 2011 Inkylabs et al.
#
#   Licensed under the Apache License, Version 2.0 (the "License");
#   you may not use this file except in compliance with the License.
#   You may obtain a copy of the License at
#
#       http://www.apache.org/licenses/LICENSE-2.0
#
#   Unless required by applicable law or agreed to in writing, software
#   distributed under the License is distributed on an "AS IS" BASIS,
#   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
#   See the License for the specific language governing permissions and
#   limitations under the License.
#
from .character import Character
from .settings import MAX_STORY
from .strings import FRED, FMAG, HC, RS, elements
from .weapon import generate_weapon

from random import choice, randint
from math import sqrt, ceil

class Monster(Character):
    def __init__(self, room=None, floor=None, health=10, strength=5,
                 defense=0, speed=5, name='Generic Bad Guy', exp=0,
                 level=1, element='fire'):
        super(Monster, self).__init__(room, floor, health, strength,
            defense, speed, name, exp, level)
        self.element = element

    def __str__(self):
        return (HC + FRED + self.element + ' ' +
                super(Monster, self).__str__() + RS)

    def fancy_str(self):
        return (str(self) + ' (health ' + HC + FMAG +
            ('%d/%d' % (self.health, self.max_health)) + RS + ', lvl %d'
            % self.level + ')')

def generate_monster(story):
    rand = ceil(sqrt(story))
    names = ['werewolf', 'vampire', 'ninja']
    level = story + randint(-rand, rand)
    health   = 20 + randint(-3, 3) + (50 * level/MAX_STORY)
    strength = 5  + randint(-1, 1) + (10 * level/MAX_STORY)
    defense  = 5  + randint(-1, 1) + (10 * level/MAX_STORY)
    speed    = 5  + randint(-1, 1) + (10 * level/MAX_STORY)
    monster  = Monster(health=health, strength=strength, defense=defense,
                      speed=speed, name=choice(names), level=level,
                      element=choice(elements))
    monster.weapon = generate_weapon(story)
    monster.inventory.append(monster.weapon)
    return monster
