#
#   Copyright 2011 Inkylabs et al.
#
#   Licensed under the Apache License, Version 2.0 (the "License");
#   you may not use this file except in compliance with the License.
#   You may obtain a copy of the License at
#
#       http://www.apache.org/licenses/LICENSE-2.0
#
#   Unless required by applicable law or agreed to in writing, software
#   distributed under the License is distributed on an "AS IS" BASIS,
#   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
#   See the License for the specific language governing permissions and
#   limitations under the License.
#
from .strings import armor_names, elements

from random import choice, randint, random


def generate_armor(story):
    effects = {choice(['strength', 'defense', 'speed']): story + randint(0, 2)}
    if random() < .2:
        effects[choice(elements) + ' protection'] = (int(story / 3) +
                                                     randint(0, 2))
    return Armor(choice(armor_names), effects)

class Armor(object):
    def __init__(self, name='CARDBOARD SHIELD', effects=None):
        self.name = name
        self.effects = effects or {}

    def __str__(self):
        ret = self.name
        if self.effects:
            ret += ' (' + ', '.join(['%s +%d' % (k, v)
                                    for k, v in self.effects.items()]) + ')'
        return ret

    def fancy_str(self):
        return str(self)

    def serialize(self):
        return {
            'effects': self.effects,
            'name': self.name,
            'type': 'armor',
        }

    def deserialize(self, obj):
        self.effects = obj['effects']
        self.name = obj['name']
        return self
