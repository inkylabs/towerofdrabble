#
#   Copyright 2011 Inkylabs et al.
#
#   Licensed under the Apache License, Version 2.0 (the "License");
#   you may not use this file except in compliance with the License.
#   You may obtain a copy of the License at
#
#       http://www.apache.org/licenses/LICENSE-2.0
#
#   Unless required by applicable law or agreed to in writing, software
#   distributed under the License is distributed on an "AS IS" BASIS,
#   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
#   See the License for the specific language governing permissions and
#   limitations under the License.
#
from .utils import deserialize_item, index_of
from .strings import FMAG, HC, RS, elements

from random import randint


class Character(object):
    def __init__(self, room, floor, max_health, strength, defense=0,
            speed=1, name='Guy Fawkes', exp=0, level=1, level_ups=0):
        self.room = room
        self.floor = floor

        self.name = name
        self.health = max_health
        self.max_health = max_health
        self.strength = strength
        self.defense = defense
        self.speed = speed
        self.exp = exp
        self.level = level
        self.level_ups = level_ups

        self.inventory = []
        self.weapon = None
        self.armor = []
        self.max_armor = 3

    def __str__(self):
        return self.name

    def fancy_str(self):
        return (str(self) + ' (health ' + HC + FMAG +
            ('%d/%d' % (self.health, self.max_health)) + RS + ')')

    def stop_escape(self, character):
        return 2*randint(0, self.stop_value()) > character.escape_value()

    def escape_value(self):
        return self.speed + self.damage

    def stop_value(self):
        return self.speed + self.strength

    def unequip(self, num):
        item = None
        if not self.weapon:
            num += 1
        if num == 0:
            item = self.weapon
            self.weapon = None
        else:
            item = self.armor.pop(num - 1)
        print(item.name + ' unequipped')

    def attack(self, character):
        print("%s attacks %s in a blind rage!" % (self, character))
        hit = True
        damage = 0
        if hit:
            element = self.element if self.element else self.weapon.element
            damage = character.take_damage(self.damage, element)
            print('%s hits %s in a blind rage! %s takes %d damage!'
                    % (self, character, character, damage))
            print(character.fancy_str())
        else:
            print('%s attacks in a *blind* rage and misses!' % self)
        return damage

    @property
    def equipped(self):
        return [self.weapon] + self.armor if self.weapon else self.armor

    @property
    def damage(self):
        attack = self.strength
        if self.weapon:
            attack += self.weapon.damage
        if self.armor:
            for item in self.armor:
                if 'attack' in item.effects:
                    attack += item.effects['attack']
        return (attack / 2) + randint(0, attack)

    def take_damage(self, damage, element):
        if self.armor:
            for item in self.armor:
                if 'defense' in item.effects:
                    damage -= item.effects['defense']
                damage -= item.effects.get('defense', 0)
                damage -= item.effects.get(element + ' protection', 0)
        damage = damage * (100 - self.defense)/100
        damage = min(damage, self.health)
        
        if hasattr(self, 'element'):
            if (elements.index(element) ==
                (elements.index(self.element) + 1) % len(elements)):
                damage *= 1.3
            elif (elements.index(element) ==
                  (elements.index(self.element) + 2) % len(elements)):
                damage *= 0.7
            damage = int(damage)
        self.health -= damage
        return damage

    def get_kill_exp(self, character):
        diff = (character.level - self.level)
        exp = int(100 * character.level * (2 ** diff))
        return max(exp, 0)

    def increase_exp(self, exp):
        self.exp += exp
        while self.exp >= 1000*self.level:
            self.exp -= 1000*self.level
            self.level += 1
            self.level_ups += 1
            print("You have leveled up!\nType 'level' to level up!")

    def serialize(self):
        return {
            'name': self.name,
            'health': self.health,
            'inventory': [x.serialize() for x in self.inventory],
            'weapon': index_of(self.inventory, self.weapon),
            'armor': [index_of(self.inventory, a) for a in self.armor],
            'max_health': self.max_health,
            'max_armor': self.max_armor,
            'speed': self.speed,
            'strength': self.strength,
            'defense': self.defense,
            'exp': self.exp,
            'level': self.level,
            'level_ups': self.level_ups,
        }

    def deserialize(self, obj, room, floor):
        self.room = room
        self.floor = floor
        self.name = obj['name']
        self.health = obj['health']
        self.max_armor = obj['max_armor']
        self.inventory = [deserialize_item(i) for i in obj['inventory']]
        if obj['weapon'] >= 0:
            self.weapon = self.inventory[obj['weapon']]
        self.armor = []
        for a in obj['armor']:
            if a >= 0:
                self.armor.append(self.inventory[a])
        self.max_health = obj['max_health']
        self.speed = obj['speed']
        self.strength = obj['strength']
        self.defense = obj['defense']
        self.exp = obj['exp']
        self.level = obj['level']
        self.level_ups = obj['level_ups']
        return self
