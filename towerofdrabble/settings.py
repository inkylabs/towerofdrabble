#!/usr/bin/env python
#
#   Copyright 2011 Inkylabs et al.
#
#   Licensed under the Apache License, Version 2.0 (the "License");
#   you may not use this file except in compliance with the License.
#   You may obtain a copy of the License at
#
#       http://www.apache.org/licenses/LICENSE-2.0
#
#   Unless required by applicable law or agreed to in writing, software
#   distributed under the License is distributed on an "AS IS" BASIS,
#   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
#   See the License for the specific language governing permissions and
#   limitations under the License.
#
from os import listdir, makedirs
from os.path import expanduser, join as path_join
from random import seed


MAX_STORY = 20
WAIT_TIME = 0
DEBUG = False
CONF_DIR = path_join(expanduser('~'), '.config', 'towerofdrabble')
SAVE_DIR = path_join(CONF_DIR, 'saves')
try:
    makedirs(SAVE_DIR)
except OSError:
    pass


def set_debug(val):
    global DEBUG
    DEBUG = bool(val)
    if DEBUG:
        seed(0)


def game_file(name, mode):
    return open(path_join(SAVE_DIR, name) + '.json', mode)


def readable_game_file(name):
    return game_file(name, 'r')


def writable_game_file(name):
    return game_file(name, 'w')


def game_file_names():
    for name in sorted(listdir(SAVE_DIR)):
        if name.endswith('.json'):
            yield name[:-5]
