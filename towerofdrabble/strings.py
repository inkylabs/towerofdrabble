#!/usr/bin/env python
#
#   Copyright 2011 Inkylabs et al.
#
#   Licensed under the Apache License, Version 2.0 (the "License");
#   you may not use this file except in compliance with the License.
#   You may obtain a copy of the License at
#
#       http://www.apache.org/licenses/LICENSE-2.0
#
#   Unless required by applicable law or agreed to in writing, software
#   distributed under the License is distributed on an "AS IS" BASIS,
#   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
#   See the License for the specific language governing permissions and
#   limitations under the License.
#
attack_no_target = [
    'You fire your weapon in a blind rage, hitting nothing.',
    'You punch the floor.',
    'You kick the wall.',
    'You flail your arms around, accidentally pulling a muscle.',
    'You charge forward, bellowing, but the only thing you hit is a ming '
        'dynasty \nvase.  Smooth move ex-lax.',
]

attack_miss = [
    'You try in vain to attack the %s, but to no avail',
    "You attack the %s!  But your hand slips, resulting in a miss!",
    'Your hitting the %s is a lie',
]

attack_hit = [
    'You strike the %s with righteousness'
]

player_death = [
    'That was awesome!  That %s totally pwned you, man.',
    'You were eaten by a grue-- er, I mean a %s.',
    'Killed by a %s; what a way to go.',
]
level_choices = [
    'Health (More HP)',
    'Strength (More damage dealt)',
    'Defense (Less damage taken)',
    'Speed (Have more turns)',
]

# ITEM NAMES
armor_names = [
    'CLOWN NOSE',
    'LEATHER CHAPS',
    'UGGS',
    'HOOP EARRINGS',
    'WIG',
    'CARDBOARD SHIELD',
    'MJ GLOVE',
    'TOE RING'
]


weapon_names = [
    'POTATO GUN',
    'BB GUN',
    'CANNON',
    'SLINGSHOT',
    'RUBBER BAND'
]


# Read as: is effective against
elements = [
    'water',
    'fire',
    'earth',
]


RS='\033[0m'    # reset
HC='\033[1m'    # hicolor
UL='\033[4m'    # underline
INV='\033[7m'   # inverse background and foreground
FBLK='\033[30m' # foreground black
FRED='\033[31m' # foreground red
FGRN='\033[32m' # foreground green
FYEL='\033[33m' # foreground yellow
FBLU='\033[34m' # foreground blue
FMAG='\033[35m' # foreground magenta
FCYN='\033[36m' # foreground cyan
FWHT='\033[37m' # foreground white
BBLK='\033[40m' # background black
BRED='\033[41m' # background red
BGRN='\033[42m' # background green
BYEL='\033[43m' # background yellow
BBLU='\033[44m' # background blue
BMAG='\033[45m' # background magenta
BCYN='\033[46m' # background cyan
BWHT='\033[47m' # background white
