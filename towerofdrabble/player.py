#
#   Copyright 2011 Inkylabs et al.
#
#   Licensed under the Apache License, Version 2.0 (the "License");
#   you may not use this file except in compliance with the License.
#   You may obtain a copy of the License at
#
#       http://www.apache.org/licenses/LICENSE-2.0
#
#   Unless required by applicable law or agreed to in writing, software
#   distributed under the License is distributed on an "AS IS" BASIS,
#   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
#   See the License for the specific language governing permissions and
#   limitations under the License.
#
from .character import Character
from .strings import FGRN, FMAG, HC, RS


class Player(Character):
    def __init__(self, room=None, floor=None, max_health=50, strength=5,
                defense=1, speed=6, name='Anon', exp=0, level=1, level_ups=0):
        super(Player, self).__init__(room, floor, max_health, strength,
                                     defense, speed, name, exp, level)
        self.stats = {
            'points': 0,
            'highest floor': 1,
            'highest level': 1,
            'items taken': 0,
            'monsters killed': 0,
            'damage dealt': 0,
            'most damage with one blow': 0,
            'damage taken': 0,
            'rests taken': 0,
        }

    def __str__(self):
        return HC + FGRN + super(Player, self).__str__() + RS

    def fancy_str(self):
        return (str(self) + ' (health ' + HC + FMAG +
            ('%d/%d' % (self.health, self.max_health)) + RS + ', lvl %d'
            % self.level + ', exp %d' % self.exp + '/%d)' % (1000 * self.level))

    def change_room(self, room):
        self.room = room
        if len(room.monsters):
            print('Monsters:')
        for monster in room.monsters:
            print('  ' + monster.fancy_str())
        if len(room.items):
            print('Items:')
        for item in room.items:
            print('  ' + item.fancy_str())
        if len(room.barrels):
            print('Barrels:')
        for barrel in room.barrels:
            print('  ' + barrel.fancy_str())

    def serialize(self):
        ret = super(Player, self).serialize()
        ret['stats'] = self.stats
        ret['floor'] = self.floor.story
        ret['room'] = self.room.pos
        return ret

    def deserialize(self, obj, floors):
        i = obj['floor'] - 1
        j, k = obj['room']
        super(Player, self).deserialize(obj, floors[i].rooms[j][k], floors[i])
        self.stats = obj['stats']
        return self
