#!/usr/bin/env python
#
#   Copyright 2011 Inkylabs et al.
#
#   Licensed under the Apache License, Version 2.0 (the "License");
#   you may not use this file except in compliance with the License.
#   You may obtain a copy of the License at
#
#       http://www.apache.org/licenses/LICENSE-2.0
#
#   Unless required by applicable law or agreed to in writing, software
#   distributed under the License is distributed on an "AS IS" BASIS,
#   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
#   See the License for the specific language governing permissions and
#   limitations under the License.
#
from __future__ import print_function

from .room import Room, generate_room
from .settings import MAX_STORY
from .strings import FGRN, HC, RS

from random import randint


def _generate_rooms(story, width, height):
    rooms = []
    for x in range(width):
        rooms.append([])
        for y in range (height):
            rooms[x].append(generate_room(story, (x, y)))
    return rooms


def _create_exit_path(floor):
    num_doors = 0
    up = list(floor.up_room.pos)
    down = cur_room = list(floor.down_room.pos)
    while cur_room != up:
        # Move_dir is 0 for movement in the x direction, 1 for the y direction
        move_dir = randint(0, 1)
        # If the direction we chose is already correct, switch directions
        if cur_room[move_dir] == up[move_dir]:
            move_dir = (move_dir + 1) % 2
        # Here we make the appropriate door, and move in to the new room
        if move_dir:
            if up[1] > down[1]:
                _make_door(floor.rooms, cur_room, 's')
                cur_room[1] = cur_room[1] + 1
            else:
                _make_door(floor.rooms, cur_room, 'n')
                cur_room[1] = cur_room[1] - 1
        else:
            if up[0] > down[0]:
                _make_door(floor.rooms, cur_room, 'e')
                cur_room[0] = cur_room[0] + 1
            else:
                _make_door(floor.rooms, cur_room, 'w')
                cur_room[0] = cur_room[0] - 1
        num_doors += 1
    return num_doors


def _make_door(rooms, start, vec):
    if vec == 'n':
        rooms[start[0]][start[1]].has_north = True
        rooms[start[0]][start[1] - 1].has_south = True
    elif vec == 's':
        rooms[start[0]][start[1]].has_south = True
        rooms[start[0]][start[1] + 1].has_north = True
    elif vec == 'e':
        rooms[start[0]][start[1]].has_east = True
        rooms[start[0] + 1][start[1]].has_west = True
    else:
        rooms[start[0]][start[1]].has_west = True
        rooms[start[0] - 1][start[1]].has_east = True


def generate_floor(story):
    width, height = 6, 5
    rooms = _generate_rooms(story, width, height)
    # Place stairs
    up = (randint(0, width - 1), randint(0, height - 1))
    down = (randint(0, width - 1), randint(0, height - 1))
    up_room = rooms[up[0]][up[1]]
    down_room = rooms[down[0]][down[1]]
    if story != MAX_STORY:
        up_room.has_up = True
    if story != 1:
        down_room.has_down = True
    # Create floor, and set monsters' floors
    floor = Floor(story, rooms, up_room, down_room, width, height)
    for x in floor.rooms:
        for room in x:
            for monster in room.monsters:
                monster.floor = floor
    # Create path between stairs, and add random doors
    num_doors_created = _create_exit_path(floor)
    for i in range(55 - num_doors_created):
        room = floor.rooms[randint(0, width - 1)][randint(0, height - 1)]
        wall = randint(0, 3)
        if wall == 0:
            if room.pos[1] == 0:
                continue
            _make_door(floor.rooms, room.pos, 'n')
        elif wall == 1:
            if room.pos[1] == height - 1:
                continue
            _make_door(floor.rooms, room.pos, 's')
        elif wall == 2:
            if room.pos[0] == 0:
                continue
            _make_door(floor.rooms, room.pos, 'w')
        else:
            if room.pos[0] == width - 1:
                continue
            _make_door(floor.rooms, room.pos, 'e')
    return(floor)


class Floor(object):
    def __init__(self, story=1, rooms=None, up=None, down=None, width=1,
                 height=1):
        self.story = story
        self.rooms = rooms
        self.up_room = up
        self.down_room = down
        self.width = width
        self.height = height

    def display(self, player_room):
        """
        |-------|-------|
        |m    ^ |      v|
        |       +   X   |
        |i     b|       |
        |-------|-------|

        """
        print('|-------' * self.width, end='')
        print('|')
        for row in range(self.height):
            # Print top row
            for col in range(self.width):
                print('|', end='')
                if len(self.rooms[col][row].monsters) != 0:
                    print('m    ', end='')
                else:
                    print('     ', end='')
                if self.rooms[col][row].has_up:
                    print('^', end='')
                else:
                    print(' ', end='')
                if self.rooms[col][row].has_down:
                    print('v', end='')
                else:
                    print(' ', end='')
            print('|')

            # Print middle row
            for col in range(self.width):
                if self.rooms[col][row].has_west:
                    print('+', end='')
                else:
                    print('|', end='')
                if self.rooms[col][row] == player_room:
                    print(HC + FGRN + '   X   ' + RS, end='')
                else:
                    print('       ', end='')
            print('|')

            # Print the bottom row
            for col in range(self.width):
                print('|', end='')
                if len(self.rooms[col][row].items) != 0:
                    print('i', end='')
                else:
                    print(' ', end='')
                if len(self.rooms[col][row].barrels) != 0:
                    print('     b', end='')
                else:
                    print('      ', end='')
            print('|')

            for col in range(self.width):
                if self.rooms[col][row].has_south:
                    print('|---+---', end='')
                else:
                    print('|-------', end='')
            print('|')

    def serialize(self):
        rooms = []
        for i, col in enumerate(self.rooms):
            if self.up_room in col:
                up_room = (i, col.index(self.up_room))
            if self.down_room in col:
                down_room = (i, col.index(self.down_room))
            rooms.append([room.serialize() for room in col])
        return {
            'rooms': rooms,
            'up_room': up_room,
            'down_room': down_room,
        }

    def deserialize(self, obj, story):
        self.story = story
        self.rooms = []
        for i, col in enumerate(obj['rooms']):
            self.rooms.append([Room().deserialize(r, self, (i, j))
                               for j, r in enumerate(col)])
            if obj['up_room'][0] == i:
                self.up_room = self.rooms[-1][obj['up_room'][1]]
            if obj['down_room'][0] == i:
                self.down_room = self.rooms[-1][obj['down_room'][1]]
        self.width = len(self.rooms)
        self.height = len(self.rooms[0])
        return self
