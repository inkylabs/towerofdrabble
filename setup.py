#!/usr/bin/env python
#
#   Copyright 2011 Inkylabs et al.
#
#   Licensed under the Apache License, Version 2.0 (the "License");
#   you may not use this file except in compliance with the License.
#   You may obtain a copy of the License at
#
#       http://www.apache.org/licenses/LICENSE-2.0
#
#   Unless required by applicable law or agreed to in writing, software
#   distributed under the License is distributed on an "AS IS" BASIS,
#   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
#   See the License for the specific language governing permissions and
#   limitations under the License.
#
from distutils.core import setup


setup(
    name='towerofdrabble',
    version='1.0',
    description='A text adventure made of awesome',
    long_description='A text adventure made of awesome',
    author='Inkylabs et al.',
    author_email='sarato@inkylabs.com',
    url='http://towerofdrabble.inkylabs.com/',
    packages=[
        'towerofdrabble',
    ],
    scripts=[
        'scripts/towerofdrabble',
    ],
    license='Apache2.0',
    platforms=['GNU/Linux', 'Windows', 'MacOS']
)
